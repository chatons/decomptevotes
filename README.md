# Décompte des votes

Ce tuto est prévu pour Linux mais 

## Téléchargement du programme généré par ppom (simple)

Télécharger le paquet `decomptevotes` de la dernière [release](https://framagit.org/chatons/decomptevotes/-/releases/)

Créer un dossier où vous voulez pour le décompte des votes et y déplacer le fichier téléchargé.
Ouvrir un terminal dans le dossier et lancer cette commande pour le rendre exécutable :
```
chmod +x decomptevotes
```

Utilisation :
```
./decomptevotes forum.chatons.org <ID>
```

## Téléchargement des sources avec git (avancé)

Tenter un `apt install golang` ou `dnf install go` ou autre, selon votre distribution Linux.

Il faut au moins `go1.21`. Vérifiable avec la commande :
```
go version
```

Utilisation :

```
go run . forum.chatons.org <ID>
```

## Utilisation

`<ID>` est le numéro du fil de vote.

Par exemple pour <https://forum.chatons.org/t/vote-clarification-sur-les-exceptions-aux-100-libre/6062/>, l'ID est 6062.

Le programme stocke en cache un fichier `.json` des votes, ça permet de ne pas se faire bannir par le forum quand on relance le programme plusieurs fois d'affilée, notamment pour le développement.
Il suffit de supprimer le fichier pour que le programme re-télécharge les votes.

Le programme affiche des informations utiles (on l'espère) sur stderr, et enregistre un `.csv`.

Pour l'instant, le programme s'arrête là : c'est à l'humain·e de faire quelque chose du CSV, en l'important dans un tableur par exemple...
