package main

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"regexp"
	"slices"
	"strings"
)

func die(err error, msg string) {
	if err != nil {
		fmt.Fprintln(os.Stderr, msg)
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

type APIResponse struct {
	Title      string `json:"title"`
	PostStream struct {
		Posts []Post `json:"posts"`
	} `json:"post_stream"`
}

type Post struct {
	Username         string `json:"username"`
	PrimaryGroupName string `json:"primary_group_name"`
	Cooked           string `json:"cooked"`
}

// map[resolution]map[groupe]mention
type GroupUser struct{ g, u string }
type Votes map[GroupUser]map[string]string

func getMention(s string) string {
	s = strings.TrimSpace(s)
	s = strings.ToLower(s)
	switch s {
	case "abstention":
		return "abstention"
	case "blanc":
		return "blanc"
	case "a rejeter", "à rejeter","a-rejeter", "à-rejeter":
		return "à rejeter"
	case "a retravailler", "à retravailler","a-retravailler", "à-retravailler":
		return "à retravailler"
	case "passable":
		return "passable"
	case "assez bien", "assez-bien":
		return "assez bien"
	case "bien":
		return "bien"
	case "très bien", "tres bien","très-bien","tres-bien":
		return "très bien"
	default:
		return "non reconnu"
	}
}

func stripChaton(chaton string) string {
	chaton = strings.ToLower(chaton)
	chaton = strings.ReplaceAll(chaton, "ê", "e")
	chaton = strings.ReplaceAll(chaton, "é", "e")
	chaton = strings.ReplaceAll(chaton, "è", "e")
	chaton = strings.ReplaceAll(chaton, "â", "a")
	chaton = strings.ReplaceAll(chaton, "à", "a")
	chaton = strings.ReplaceAll(chaton, " ", "")
	chaton = strings.ReplaceAll(chaton, "-", "")
	return chaton
}

func main() {
	if len(os.Args) != 3 {
		fmt.Fprintf(os.Stderr, "usage: command <domain> <post_id>")
		os.Exit(1)
	}
	domain := os.Args[1]
	postId := os.Args[2]

	jsonFile := fmt.Sprintf("%v:%v.json", domain, postId)
	url := fmt.Sprintf("https://%v/t/%v.json?print=true", domain, postId)

	var postsResponse APIResponse

	file, err := os.Open(jsonFile)
	if err == nil {
		// load file
		err = json.NewDecoder(file).Decode(&postsResponse)
		die(err, "Le fichier local JSON n'a pas pu être interprété. Supprimez le fichier et rééssayez.")
	} else {
		response, err := http.Get(url)
		die(err, "Le post n'a pas pu être téléchargé. Problème d'URL ou de connexion ?")

		// read body into a buffer
		buf := bytes.NewBuffer([]byte{})
		_, err = buf.ReadFrom(response.Body)
		die(err, "La réponse du serveur n'a pas pu être interprétée")

		// write to file
		err = os.WriteFile(jsonFile, buf.Bytes(), 0644)
		die(err, "Je n'ai pas pu créer de fichier dans le dossier actuel")

		// load in memory
		err = json.NewDecoder(bufio.NewReader(buf)).Decode(&postsResponse)
		die(err, "Je n'ai pas pu interpréter la réponse du serveur du forum")
	}

	fmt.Fprintf(os.Stderr, "\033[32mTitre\033[0m: %v\n", postsResponse.Title)
	posts := postsResponse.PostStream.Posts

	if len(posts) < 10 {
		fmt.Fprintf(os.Stderr, "\033[33mIl y a seulement %v post·s!\033[0m\n", len(posts))
	}

	htmlreg := regexp.MustCompile(`<[^>]*>`)
	chatonreg := regexp.MustCompile(`^(?i)Pour le chatons? ?:? ([^:]*)[ :]*$`)
	votereg := regexp.MustCompile(`^ *([^:]*) *: *([^\*]*)[ \*]*$`)

	votes := make(Votes)
	groups := make([]GroupUser, 0)
	resolutions := make(map[string]struct{}, 0)

	for i, post := range posts {
		message := htmlreg.ReplaceAllLiteralString(post.Cooked, "")
		lines := strings.Split(message, "\n")

		firstline := chatonreg.FindStringSubmatch(lines[0])
		if firstline == nil {
			if len(lines[0]) > 50 {
				lines[0] = lines[0][:50]
			}
			fmt.Fprintf(os.Stderr, "Post %-3v de \033[36m%-10v\033[31m \033[31mignoré\033[0m:     première ligne pas formattée: ~~%v~~\n", i+1, post.Username, lines[0])
			continue
		}
		chaton := strings.TrimSpace(firstline[1])

		if stripChaton(chaton) != stripChaton(post.PrimaryGroupName) {
			fmt.Fprintf(os.Stderr, "Post %-3v de \033[36m%-10v\033[31m \033[33mà vérifier\033[0m: vote pour le chaton \033[33m%v\033[0m mais son groupe sur le forum est \033[33m%v\033[0m\n", i+1, post.Username, chaton, post.PrimaryGroupName)
		}

		groupUser := GroupUser{chaton, post.Username}
		groups = append(groups, groupUser)
		votes[groupUser] = make(map[string]string)

		for _, line := range lines[1:] {
			if strings.TrimSpace(line) == "" {
				continue
			}
			lineItems := votereg.FindStringSubmatch(line)
			if lineItems == nil {
				break
			}
			resolution := strings.TrimSpace(lineItems[1])
			mention := getMention(lineItems[2])

			resolutions[resolution] = struct{}{}
			votes[groupUser][resolution] = mention
		}
	}
	// have a sorted list of resolutions
	resolutionsSlice := make([]string, 0)
	for reso := range resolutions {
		resolutionsSlice = append(resolutionsSlice, reso)
	}
	slices.SortFunc(resolutionsSlice, NatSort)
	slices.SortFunc(groups, func(a, b GroupUser) int {
		return strings.Compare(a.g, b.g)
	})

	csvFilename := fmt.Sprintf("%v:%v.csv", domain, postId)
	csvFile, err := os.OpenFile(csvFilename, os.O_CREATE|os.O_WRONLY, 0644)
	die(err, "Le fichier CSV n'a pas pu être créé")

	printer := csv.NewWriter(csvFile)

	header := []string{"chaton", "user"}
	header = append(header, resolutionsSlice...)

	printer.Write(header)

	for _, groupUser := range groups {
		vote := votes[groupUser]
		csvLine := []string{groupUser.g, groupUser.u}
		for _, reso := range resolutionsSlice {
			if vote[reso] == "" {
				vote[reso] = "non reconnu"
			}
			csvLine = append(csvLine, vote[reso])
		}
		err = printer.Write(csvLine)
		die(err, "Erreur lors de l'écriture dans le fichier CSV")
	}

	printer.Flush()
}
